PEST-icide is a 4 player third-person brawler developed in Unity as a third year Game Development Workshop project.

PEST-icide was created by G6 Studios, a team formed by Quinn Daggett, Jake Jandu, Raza Kazmi, Moishe Grosh, Jean Duchene, and Regan Tran.

Xbox controllers required to play.

Credits:
- Quinn Daggett: Gameplay programmer
- Jake Jandu: UI artist
- Raza Kazmi: Network programmer
- Moishe Grosh: Character modeller, animator
- Jean Duchene: Animator
- Regan Tran: Environment modeller

[![Gameplay footage here](/uploads/22d8852f867423a6455e429f4f11f200/pest-icide_image.png)](https://www.youtube.com/watch?v=ewOc-BC2KE0)

